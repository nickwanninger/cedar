/*
 * MIT License
 *
 * Copyright (c) 2018 Nick Wanninger
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <cedar/exception.hpp>

#include <stdio.h>
#include <cstdlib>
#include <cstdint>
#include <cstring>
#include <map>
#include <list>
#include <vector>
#include <memory.h>
#include <cedar/ref.h>

namespace cedar {

	class object;


	namespace vm {


		// bytecode object
		class bytecode {

			private:

				// size is how many bytes are written into the code pointer
				// it also determines *where* to write when writing new data
				uint64_t size = 0;
				// cap is the number of bytes allocated for the code pointer
				uint64_t cap = 255;

			public:

				int stack_size = 0;

				// constants stores values that the bytecode can reference with
				// a simple index into it.
				std::vector<ref> constants;

				// the code pointer is where this instance's bytecode is actually
				// stored. All calls for read interpret a type from this byte vector
				// and calls for write append to it.
				uint8_t *code;

				inline bytecode() {
					code = new uint8_t[255];
				}


				ref& get_const(int);
				int push_const(ref);

				void finalize();

				template<typename T>
					inline T operator[](int) {
						return {};
					}



				template<typename T>
					inline T read(uint64_t i) const {
						return *(T*)(void*)(code+i);
					}


				template<typename T>
					inline uint64_t write_to(uint64_t i, T val) const {
						if (sizeof(T) + i >= cap)
							throw cedar::make_exception("bytecode unable to write ", sizeof(T), " bytes to index ", i, ". Out of bounds.");
						*(T*)(void*)(code+i) = val;
						return size;
					}

				template<typename T>
					inline uint64_t write(T val) {
						if (size + sizeof(val) >= cap-1) {
							uint8_t *new_code = new uint8_t[cap * 2];
							std::memcpy(new_code, code, cap);
							cap *= 2;
							delete code;
							code = new_code;
						}
						uint64_t addr = write_to(size, val);
						size += sizeof(T);
						return addr;
					}

				inline uint64_t get_size() {
					return size;
				}
				inline uint64_t get_cap() {
					return cap;
				}


				template<typename T>
					inline T & at(int i) {
						return *(T*)(void*)(code+i);
					}
		};

	} // namespace vm
} // namespace cedar
